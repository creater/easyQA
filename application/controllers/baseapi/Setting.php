<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 设置接口控制器
 */
class Setting extends BaseAPI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 全屏切换
     */
    public function full_screen()
    {
        if (isset($_SESSION['full_screen']) && $_SESSION['full_screen']) {
            $_SESSION['full_screen'] = false;
        } else {
            $_SESSION['full_screen'] = true;
        }
    }
}
