<?php require_once VIEWPATH . "$theme_id/inc/header.inc.php";?>
<div class="main layui-clear">
    <div class="wrap">
        <div class="content w">
            <h2 class="page-title">发布文章内容带有 #话题# 即可生成话题（标题中含有话题无效）。</h2>
            <div class="topic-list">
                <div class="clearfix">
                    <?php if (is_array($topic_lists)): ?>
                        <?php foreach ($topic_lists as $_topic): ?>
                            <div class="item">
                                <a class="img border-radius-5" href="/topic/articles/<?=$_topic['id']?>" data-id="796">
                                    <img src="http://<?=$config['qiniu']['static_bucket_domain']?>/topic.jpg?imageView2/1/w/50/h/50/interlace/0/q/100" alt="">
                                </a>
                                <p class="clearfix">
                                    <span class="topic-tag">
                                        <a class="text" href="/topic/articles/<?=$_topic['id']?>" data-id="796"><?=$_topic['topic']?></a>
                                    </span>
                                </p>
                                <p class="text-color-999">
                                    <span><?=$_topic['used_times']?> 个讨论</span>
                                </p>
                            </div>
                        <?php endforeach;?>
                    <?php else: ?>
                        <p>无</p>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once VIEWPATH . "$theme_id/inc/footer.inc.php";?>