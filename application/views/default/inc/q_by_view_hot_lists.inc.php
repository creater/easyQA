<h3 class="page-title">最近热帖</h3>
<ol class="fly-list-one">
    <?php if (is_array($q_by_view_hot_lists)): ?>
        <?php foreach ($q_by_view_hot_lists as $_q): ?>
            <li>
                <a href="/q/detail/<?=$_q['id']?>"><?=xss_filter($_q['article_title'])?></a>
                <span><?=$_q['view_counts']?> <i class="iconfont">&#xe60b;</i></span>
            </li>
        <?php endforeach;?>
    <?php endif;?>
</ol>