<div class="topic_nav clearfix">
	<a<?=empty($topic) ? ' class="active"' : ''?> href="/<?=$active != 'all' ? $active : ''?>">全部</a>
	<?php if (!empty($topic) && !in_array($topic, $config['topic_navs'])): ?>
		<a class="active" href="/topic/articles?topic=<?=urlencode($topic)?>&type=<?=$active?>"><?=$topic?></a>
	<?php endif;?>
	<?php foreach ($config['topic_navs'] as $_v): ?>
		<a<?=$topic == $_v ? ' class="active"' : ''?> href="/topic/articles?topic=<?=urlencode($_v)?>&type=<?=$active?>"><?=$_v?></a>
	<?php endforeach;?>
	<a href="/topic">更多&raquo;</a>
</div>
